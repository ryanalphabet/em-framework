# Engineering Manager Framework
This is a collection of my own processes when it comes to being an engineering manager. It will grow and adapt as I progress in my career, and hopefully will be of help to others.

[User Guide](user-guide.md)