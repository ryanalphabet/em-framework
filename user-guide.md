# User Guide

- Conditions I like to work in

    Ideal work conditions would be a quiet and empty meeting room, not too hot and not too cold.

    I prefer not to use headphones but will when the office is noisy and I need to focus.

    I like to plan out what I work on each week and each day, so a good todo list makes me productive.

- Times and hours I like to work

    My working hours vary depending on how well my son sleeps, though I prefer to get in at around 8 and leave at 4. Getting in early has the bonus of a quiet office, as well as public transport being less gross.

    I'll generally work outside of office hours if it's on things that I enjoy doing or didn't get a chance to do during the day.

    I'm generally happy to answer any questions outside of work hours, if not I'll leave it until the next work day.

- The best ways to communicate with me

    Slack is the best way to contact me both during work and outside of office hours. If I haven't responded feel free to @ me again.

    I prefer not to be desk bombed unless it's an emergency, as I try to be organised with my time and prioritise accordingly.

    I prefer to come to a conversation or meeting prepared, so I like to know what's going to be discussed beforehand.

    I make a point to only check emails every 2 hours, so if there's something urgent it's best to not put it in an email.

    I don't like conversing via email especially in groups, Slack is best for that.

- Ways I like to receive feedback

    I'm a strong believer in immediate feedback, so prefer to give and get it straight away if there's something that needs to be addressed.

    I'm always looking to improve, so letting me know where I could be doing things better or differently will always be appreciated.

    I prefer constructive feedback face to face, however compliments are welcome anywhere!

- Things I need

    Time to prepare before a conversation.

    Feedback around what I'm doing well or what could be improved.

    Come to me with potential solutions, not just the problems.

    Autonomy. If I don't feel empowered I struggle.

    Proactive communication - I dislike asking for updates as I hate micromanaging.

- Things I struggle with

    People taking the piss.

    People that don't strive to be better.

    Moving goalposts.

- Things I love

    Seeing teamwork in action.

    Improving processes and workflows.

    Building things that improve my day to day.

    Compliments and recognition.

    Helping people and making their jobs a little bit better.

- Other things to know about me

    I'm incredibly shy, unless I know absolutely no one and then I try and talk to people.

    I have a massive computer controlled telescope. You can tell it what to look at and if it's visible it'll find it.

    I once learned the violin for a week until I broke my hand in a fall. I continued to hire the violin for 9 months.

    I like to lead inclusively, utilising the expertise and experience in my team and not just myself.